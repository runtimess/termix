const net = require('net')
const colors = require('colors')
const PORT = process.env.PORT
const HOST = process.env.HOST

class ChatClient extends net.Socket {
	constructor(port = 3000, host = 'localhost') {
		super()
		this._port = port
		this._host = host
		this._reconnectDelay = 300
		this.socket = this
		this._watcher()
		this._step = 0
		this.socket.connect(port, host)
		this._reconnecting = false
	}

	_watcher() {
		this.socket.once('connect', () => {
			const clientData = {
				id: Math.floor(Math.random() * 909090)
			}
			process.stdin.on('data', message => {
				if (!message.toString().trim().length) {
					switch(this._step) {
						case 0: return process.stdout.write('Your login: ')
						case 1: return process.stdout.write('MSG: ')
					}	
				}

				if (this._step === 0) {
					clientData.login = message.toString().split('\n')[0]
					this.socket.write(JSON.stringify({
						type: 'JOIN',
						payload: clientData
					}))
					process.stdout.write('MSG: ')
				
				}

				if (this._step === 1) {
					this.socket.write(JSON.stringify({
						type: 'MESSAGE',
						payload: {
							id: clientData.id,
							message: message.toString()
						}
					}))
					process.stdout.write('MSG: ')
				}

				if (this._step !== 1)
					this._step++
			})

			this.socket.on('data', data => {
				try {
					var data = JSON.parse(data.toString())
				} catch (ex) {
					process.stdout.write('Error: ' + ex.message)
				}
				reducer.call(this, data.type, data.payload)
			})
			this.socket.on('close', () => {
				if (!this._reconnecting) {
					this._reconnecting = true
					process.stdout.clearLine()
					process.stdout.cursorTo(0)
					console.log('\n----CONNECTION CLOSED----'.red)
					console.log('----RECONNECTIONG...'.green)
				}
				this._reconnect()
				this._step = 0
			})
			this.socket.on('error', err => {
				this._step = 0
			})
		})
		
	}

	_reconnect() {
		setTimeout(() => {
			// this.socket.destroy()
			this.socket.connect(this._port, this._host)
		}, this._reconnectDelay)
	}

	// _destroy() {
	// 	console.log('CONNECTION DEPRICATED')
	// 	this.socket.destroy()
	// }
}


const client = new ChatClient(PORT, HOST)



function reducer(type, payload) {
	if (type === 'CONNECTION_SETUP') {
		process.stdout.clearLine()
		process.stdout.cursorTo(0)
		console.log('You join to chat'.green)
		process.stdout.write('Your login: ')
		this._reconnecting = false
	}

	if (type === 'MESSAGE') {
		process.stdout.clearLine()
		process.stdout.cursorTo(0)
		process.stdout.write(payload.login.green + ': ' + payload.message)
		process.stdout.write('MSG: ')
	}

	if (type === 'NEW_CLIENT') {
		process.stdout.clearLine()
		process.stdout.cursorTo(0)
		process.stdout.write(`----${payload.login} JOIN----\n`.green)
		process.stdout.write('MSG: ')
	}

	if (type === 'DISCONNECTED_CLIENT') {
		process.stdout.clearLine()
		process.stdout.cursorTo(0)
		process.stdout.write(`----${payload.login} LEAVE----\n`.red)
		process.stdout.write('MSG: ')
	}
}
