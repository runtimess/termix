const net = require('net')
const clients = []
const PORT = process.env.PORT || 3000
const HOST = process.env.HOST || 'localhost'


// SERVER
net.Server(socket => {

	if (!clients.includes(socket)) {
		socket.write(JSON.stringify({
			type: 'CONNECTION_SETUP'
		}))
	}

	process.stdin.on('data', message => {
		message = message.toString().split('\n')[0]
		if (message === '/killall') {
			clients.forEach(c => c.destroy())
		}
		console.log('MESSSAGE', message)
	})
	// Data handler
	socket.on('data', clientDataHandler.bind(null, socket))
	// Disconnect handler
	socket.on('end', clientDisconnect.bind(null, socket))
}).listen(PORT, () => console.log('CHAT LIVE ON PORT ' + PORT))



// Emit
function emit(socket, data) {
	socket.write(JSON.stringify(data))
}
// Broadcast
function broadcast(socket, data) {
	clients.forEach(c => {
		if (c === socket) return
			emit(c, data)
	})
}

function clientDataHandler(socket, data) {
	try {
		var data = JSON.parse(data.toString())
		// console.log('DATA', data)
	} catch(ex) {
		return emit(socket, {
			type: 'ERROR'
		})
	}

	if (data.type === 'JOIN') {
		console.log('New user joined', data.payload.login, data.payload.id)
		socket.login = data.payload.login
		socket.id = data.payload.id
		clients.push(socket)
		broadcast(socket, {
			type: 'NEW_CLIENT',
			payload: {
				login: socket.login
			}
		})
	} else if (data.type === 'MESSAGE') {
		broadcast(socket, {
			type: 'MESSAGE',
			payload: {
				login: socket.login,
				message: data.payload.message
			}
		})
		process.stdout.write(socket.login + ': ' + data.payload.message)
	}
}
function clientDisconnect(socket) {
	if (!clients.includes(socket)) return
	console.log(socket.login, 'DISCONNECTED')
	broadcast(socket, {
		type: 'DISCONNECTED_CLIENT',
		payload: {
			login: socket.login
		}
	})
	clients.splice(clients.indexOf(socket), 1)
}
